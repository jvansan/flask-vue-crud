# flask-vue-crud

A simple Flask app with Vue.js frontend.

Learned from https://testdriven.io/blog/developing-a-single-page-app-with-flask-and-vuejs/#alert-component and deployed to [heroku](https://jvansan-books.herokuapp.com).
