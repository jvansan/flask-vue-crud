import uuid

from flask import Flask, jsonify, request

from flask_cors import CORS

DEBUG = True
BOOKS = [
        {
            'id': uuid.uuid4().hex,
            "title": "On the Road",
            "author": "Jack Kerouac",
            "read": True,
        },
        {
            'id': uuid.uuid4().hex,
            "title": "Harry Potter and the Philosopher's Stone",
            "author": "J. K. Rowling",
            "read": False,
        },
        {
            'id': uuid.uuid4().hex,
            "title": "Green Eggs and Ham",
            "author": "Dr. Seuss",
            "read": True,
        },
]

def remove_book(book_id):
    for book in BOOKS:
        if book['id'] == book_id:
            BOOKS.remove(book)
            return True
    return False

app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*':{'origins': '*'}})

@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

@app.route('/books', methods=['GET', 'POST'])
def all_books():
    response = {"status": "success"}
    if request.method == "POST":
        post_data = request.get_json()
        BOOKS.append({
            'id': uuid.uuid4().hex,
            "title": post_data.get("title"),
            "author": post_data.get("author"),
            "read": post_data.get("read"),
        })
        response["message"] = "Book added!"
    else:
        response["books"] = BOOKS
    return jsonify(response)

@app.route('/books/<book_id>', methods=['PUT', 'DELETE'])
def single_book(book_id: int):
    response = {"status": "success"}
    if request.method == "PUT":
        data = request.get_json()
        remove_book(book_id)
        BOOKS.append({
            'id': uuid.uuid4().hex,
            "title": data.get("title"),
            "author": data.get("author"),
            "read": data.get("read"),
        })
        response["message"] = "Book updated!"
    if request.method == 'DELETE':
        remove_book(book_id)
        response["message"] = "Book removed!"
    return jsonify(response)


if __name__ == '__main__':
    app.run()
